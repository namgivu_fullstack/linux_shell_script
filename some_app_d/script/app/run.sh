run() {
    echo -e "This is\n    some_app_d/script/app/run.sh"

    local SH=`cd ${BASH_SOURCE%/*} && pwd`  #NOTE this works w/ bash shell BUT NOT WORK w/ sh shell
    echo "SH=$SH"

    # call s00/run.sh and  s01/run.sh
    source "$SH/../../service/s00/up.sh"
    source "$SH/../../service/s01/up.sh"
}
    run
