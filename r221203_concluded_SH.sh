output_f="$(dirname $BASH_SOURCE)/r221203_concluded_SH.sh.txt" ; echo '' > $output_f

linuxdistro_i='python:3.8'
with_bashshell_i="python3.8__with_bashshell_i"
    echo -e "\n--- Running w/ $linuxdistro_i $shell_cmd" | tee -a $output_f

    #region build dockerimage that has bash shell
    Dockerfile_f="$(dirname $BASH_SOURCE)/r221203_Dockerfile_${with_bashshell_i}"
        cat << EOT > $Dockerfile_f
FROM $linuxdistro_i

# install bash shell ref. https://stackoverflow.com/a/40944512/248616
RUN apt-get update && apt-get -y install bash
RUN bash --version && which bash
EOT
        docker build -t $with_bashshell_i -f $Dockerfile_f .
    #endregion build dockerimage that has bash shell

    f1="$(dirname $BASH_SOURCE)/zprint_SH.sh"
    f2="$(dirname $BASH_SOURCE)/some_app_d" ; some_app_d_PathInContainer='/some_app_d'
    c='linuxdistro_c'
        docker rm -f         $c || echo
        docker run -d --name $c -h$c \
            -v "$f1:/zprint_concluded_SH.sh"   \
            -v "$f2:$some_app_d_PathInContainer" \
            -e some_app_d_PathInContainer=$some_app_d_PathInContainer \
            $with_bashshell_i \
                tail -f

        docker exec $c  bash '/zprint_concluded_SH.sh' | tee -a $output_f



echo -e "\n\n ------ ------ ------\n"



linuxdistro_i='alpine'
with_bashshell_i="alpine__with_bashshell_i"
    echo -e "\n--- Running w/ $linuxdistro_i $shell_cmd" | tee -a $output_f

    #region build dockerimage that has bash shell
    Dockerfile_f="$(dirname $BASH_SOURCE)/r221203_Dockerfile_${with_bashshell_i}"
        cat << EOT > $Dockerfile_f
FROM $linuxdistro_i

# install bash shell ref. https://stackoverflow.com/a/40944512/248616
RUN apk update && apk add bash
RUN bash --version && which bash
EOT
        docker build -t $with_bashshell_i -f $Dockerfile_f .
    #endregion build dockerimage that has bash shell

    f1="$(dirname $BASH_SOURCE)/zprint_concluded_SH.sh"
    f2="$(dirname $BASH_SOURCE)/some_app_d" ; some_app_d_PathInContainer='/some_app_d'
    c='linuxdistro_c'
        docker rm -f         $c || echo
        docker run -d --name $c -h$c \
            -v "$f1:/zprint_concluded_SH.sh"   \
            -v "$f2:$some_app_d_PathInContainer" \
            -e some_app_d_PathInContainer=$some_app_d_PathInContainer \
            $with_bashshell_i \
                tail -f

        docker exec $c  bash '/zprint_concluded_SH.sh' | tee -a $output_f

# strip the output
python << EOT
fc=open('$output_f')     .read().strip()+'\n'
open   ('$output_f', 'w').write(fc)
EOT

conclusion='
in sh shell, there seems to be NO ALTERNATIVE for $BASH_SOURCE as in bash shell
--> just install bash when you dont have it
'
