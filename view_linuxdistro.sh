# command to view current linux distro ref. https://unix.stackexchange.com/a/10288/17671
echo; cmd='cat /etc/*version'; echo "Running $cmd" ; eval $cmd
echo; cmd='cat /proc/version'; echo "Running $cmd" ; eval $cmd
echo; cmd='cat /etc/*release'; echo "Running $cmd" ; eval $cmd
echo; cmd='uname -rv';         echo "Running $cmd" ; eval $cmd

echo; echo "python version"   ; python3 -V
echo; echo "current shell"    ; cat /etc/shells  # ref. https://www.cyberciti.biz/tips/how-do-i-find-out-what-shell-im-using.html
