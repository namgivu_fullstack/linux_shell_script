linuxdistro_i='python:3.8'
    f="$(dirname $BASH_SOURCE)/view_linuxdistro.sh"
    c='linuxdistro_c'
        docker rm -f      $c || echo
        docker run -d --name $c -h$c  -v "$f:/view_linuxdistro.sh"  $linuxdistro_i tail -f

        docker exec $c bash '/view_linuxdistro.sh'

    consoleoutput='
    PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"

    python version
    Python 3.8.13

    current shell
        PID TTY          TIME CMD
          6 ?        00:00:00 bash
    '

echo ------ ------ ------

linuxdistro_i='alpine'
    f="$(dirname $BASH_SOURCE)/view_linuxdistro.sh"
    c='linuxdistro_c'
        docker rm -f         $c || echo
        docker run -d --name $c -h$c  -v "$f:/view_linuxdistro.sh"  $linuxdistro_i tail -f

        docker exec $c sh '/view_linuxdistro.sh'

    consoleoutput='
    PRETTY_NAME="Alpine Linux v3.16"

    python version
    /view_linuxdistro.sh: line 7: python3: not found

    current shell
    # valid login shells
    /bin/sh
    /bin/ash
    '

echo ------ ------ ------
echo "
Conclusion
use python:3.8 to run bash shell
use alpine     to run sh   shell
"
