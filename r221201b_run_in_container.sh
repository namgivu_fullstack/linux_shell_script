output_f="$(dirname $BASH_SOURCE)/r221201b_run_in_container.sh.txt" ; echo '' > $output_f

linuxdistro_i='python:3.8' ; shell_cmd='bash'
    echo -e "\n--- Running w/ $linuxdistro_i $shell_cmd" | tee -a $output_f

    f1="$(dirname $BASH_SOURCE)/zprint_SH.sh"
    f2="$(dirname $BASH_SOURCE)/some_app_d" ; some_app_d_PathInContainer='/some_app_d'
    c='linuxdistro_c'
        docker rm -f         $c || echo
        docker run -d --name $c -h$c \
            -v "$f1:/zprint_SH.sh"   \
            -v "$f2:$some_app_d_PathInContainer" \
            -e some_app_d_PathInContainer=$some_app_d_PathInContainer \
            $linuxdistro_i \
                tail -f

        docker exec $c  $shell_cmd '/zprint_SH.sh' | tee -a $output_f

echo -e "\n ------ ------ ------"

linuxdistro_i='alpine' ; shell_cmd='sh'
    echo -e "\n--- Running w/ $linuxdistro_i $shell_cmd" | tee -a $output_f

    f1="$(dirname $BASH_SOURCE)/zprint_SH.sh"
    f2="$(dirname $BASH_SOURCE)/some_app_d" ; some_app_d_PathInContainer='/some_app_d'
    c='linuxdistro_c'
        docker rm -f         $c || echo
        docker run -d --name $c -h$c \
            -v "$f1:/zprint_SH.sh"   \
            -v "$f2:$some_app_d_PathInContainer" \
            -e some_app_d_PathInContainer=$some_app_d_PathInContainer \
            $linuxdistro_i \
                tail -f

        docker exec $c  $shell_cmd '/zprint_SH.sh' | tee -a $output_f

# strip the output
python << EOT
fc=open('$output_f')     .read().strip()+'\n'
open   ('$output_f', 'w').write(fc)
EOT

conclusion='
with SH=$(cd ${BASH_SOURCE%/*} && pwd)
we get
    distro      SH
    ----------  --------------------------
    alpine      SH=/root
    python:3.8  SH=/some_app_d/service/s01

in sh shell, there seems to be NO ALTERNATIVE for $BASH_SOURCE as in bash shell
--> just install bash when you dont have it
'
